<?php

/**
 * @file
 * Block view functions for Domain HQ.
 */

/**
 * A domain switcher for domain administrators. Differs from the block 
 * provided by Domain Access in that domains display in a dropdown and also
 * will only include domains the current user has administrator access to.
 *
 * @see domain_hq_block_view()
 * @see domain_hq_domain_switcher_form()
 * @see domain_hq_domain_switcher_form_submit()
 */
function domain_hq_block_view_switcher() {
  $block = array(
    'subject' => t('Domain switcher'),
    'content' => drupal_get_form('domain_hq_domain_switcher_form'),
  );
  return $block;
}

/**
 * Domain switcher form.
 *
 * @see domain_hq_block_view_switcher()
 * @see domain_hq_domain_switcher_form_submit()
 */
function domain_hq_domain_switcher_form($form, &$form_state) {
  global $user;
  $_domain = domain_get_domain();
  
  if (!isset($user->domain_user)) {
    $user->domain_user = domain_get_user_domains($user);
  }

  $form = array();
  
  $options = array();
  foreach (domain_domains() as $data) {
    $pass = FALSE;
    // Check domain access for users assigned to domain.
    if (isset($user->domain_user[$data['domain_id']])) {
      $pass = $data['valid'] || user_access('access inactive domains'); 
    }
    else {
      // If not assigned, only pass for user 1 or those with privileged access.
      $pass = $user->uid == 1 || user_access('set domain access');
    }
    if ($pass) {
      $options[$data['domain_id']] = $data['sitename'];
    }
  }
  
  if (empty($options)) {
    $options = array('0' => t('No domains.'));
  }
  
  $form['domain_hq_domain_switcher'] = array();
  $form['domain_hq_domain_switcher']['domains'] = array(
    '#type' => 'select',  
    '#options' => $options,
    '#default_value' => $_domain['domain_id'],
    '#prefix' => '<div class="container-inline">',
  );
  $form['domain_hq_domain_switcher']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Switch domain'),
    '#submit' => array('domain_hq_domain_switcher_form_submit'),
    '#suffix' => '</div>',
  );
  
  return $form;
}

/**
 * Process domain_hq_domain_switcher form submissions.
 *
 * @see domain_hq_block_view_switcher()
 * @see domain_hq_domain_switcher_form()
 */
function domain_hq_domain_switcher_form_submit($form, &$form_state) {
  $domain_id = $form_state['values']['domains'];
  if (isset($domain_id) && $domain_id) {
    $domain = domain_lookup($domain_id);
    drupal_goto(domain_get_uri($domain), array('external' => TRUE));
  }
}
