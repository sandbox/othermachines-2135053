/**
 * @file
 * README file for Domain HQ.
 */

Domain HQ

This module extends the Domain Access module [1] by allowing more centralized
management of multiple domains. 

Details
-------

* New option to restrict editing privileges on a node. 
  Configure 'Restrict editing privileges' to permit only editors 
  assigned to the default domain or with "set domain access" permission 
  to update or delete the content. Unless they have "set domain access" 
  permission, these editors must also be assigned to the domain to 
  which the node is assigned.
  
* Default domain is separate from "all affiliates". 
  Content assigned to 'all affiliates' will not display on default domain
  by default (must be explicitly granted).
  
* Access to administrator pages limited by domain access.
  Restricts access to admin pages to editors who are assigned to
  the current domain. If a domain administrator ends up in a domain to which
  they don't belong, they will be redirected to their account information
  page. This redirect will not be enforced for users with "set domain access" 
  permission.
  
* Block administration extension to Domain Blocks[3]. 
  If Domain Blocks module is enabled, will restrict configuration access
  according to whether the block is assigned to the domain. This allows
  domain editors with the 'administer blocks' permission to manage their 
  own blocks. When an editor creates a block - unless they have the "set 
  domain access" permission - the block will be silently assigned to the 
  current domain. 
  
* Domain switcher dropdown block. 
  New block: 'Domain HQ switcher (dropdown with access check)': differs 
  from the block provided by Domain Access in that domains display in a 
  dropdown and will only include domains the current user has administrator 
  access to. User 1 and users with "set domain access" permission will see 
  all domains.
  
Also:

  User account information page: will show "Assigned domains" to all users
  assigned to a domain. This is for convenience should they end up in the 
  wrong domain and are consequently redirected here. 

Recommended settings
--------------------

* Set "Enforce rules on Administrator" to "Restrict node views for 
  administrators" at admin/structure/domain/settings. This affects users with 
  "administer nodes" permission and user 1. 
  
* Enable this module's block, "Domain HQ switcher (dropdown with access 
  check)" in the admin theme (especially useful on content overview page at
  admin/content and, if using the Domain Blocks module, the blocks 
  administration page at admin/structure/block).

* Permissions for content editors:

  Users with these permissions can edit nodes granted to any domain to 
  which they are assigned unless 'Restrict editing privileges' is set on the node.

  Users with these permissions AND WHO ARE ASSIGNED TO THE DEFAULT DOMAIN
  can edit any nodes granted to any domain to which they are assigned.
  
  Users with these permissions can only access the admin pages in domains
  to which they are assigned. 

  BLOCK
  
  Administer blocks                               | x | (if using Domain Block)
  
  DOMAIN ACCESS

  Administer domain records and settings          |   |
  Access inactive domains                         |   | 
  Assign editors to domains                       |   |
  Set domain access status for all content        |   |
  Publish content to any assigned domain          | x |
  Publish content only from assigned domain       |   |
  Publish content only from the default domain    |   |
  Edit any content on assigned domains            | x |
  Delete any content on assigned domains          | x |
  View unpublished content on assigned domains    | x |
  [type]: Create new content on assigned domains  | x |
  [type]: Edit any content on assigned domains    | x |
  [type]: Delete any content on assigned domains  | x |

  NODE

  Bypass content access control                   |   |
  Administer content types                        |   |
  Administer content                              |   |
  Access the content overview page                | x |
  View published content                          | x |
  View own unpublished content                    |   |
  View content revisions                          |   |
  Revert content revisions                        |   |
  Delete content revisions                        |   |
  [type]: Create new content                      |   |
  [type]: Edit own content                        |   |
  [type]: Edit any content*                       |   | 
  [type]: Delete own content                      |   |
  [type]: Delete any content                      |   |

  * Note: Selecting "[type]: Edit any content" will allow your editors to edit
    any content assigned to 'all affiliates' except where 'Restrict editing 
    privileges' is enforced.
  
Issues
------

* Awaiting this feature (patched and waiting for review):
  "Allow domains to opt-out of 'all affiliates'" [2]
  Once committed, can remove code restricting 'all affiliates'
  content and instead just recommend a setting. 

* Deal with adding and deleting blocks. 

* "Add block" action button missing from default theme (but appears in admin
  theme) despite access callback returning true on admin/structure/block/add.  

Created by othermachines.

[1] http://drupal.org/project/domain
[2] https://drupal.org/node/1247746
[3] https://drupal.org/project/domain_blocks